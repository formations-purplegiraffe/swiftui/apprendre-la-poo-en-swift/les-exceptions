//
//  main.swift
//  exceptions
//
//  Created by Enrick Enet on 31/12/2020.
//

import Foundation

func afficherHtml (urlSite:URL){
    do {
        let html:String
        html = try String(contentsOf: urlSite)
        print(html)
    } catch {
        //infos sur l'erreur
        print(error.localizedDescription)
    }
}


func afficherHtmlOptionnel(urlSite:URL){
    if let html = try? String(contentsOf: urlSite){
        print(html)
    }
}

struct EmptyPageError : Error {
    //On explique dans ce struct toutes les infos de l'erreur
}

func afficherHtmlExceptions(urlSite:URL) throws {
    let html:String
    html = try String(contentsOf: urlSite)
    if html.count == 0 {
        throw EmptyPageError()
    }
    print(html)
}

func obtenirHtml(urlSite:URL) -> Result<String,Error> {
    let result:Result<String,Error>
    do {
        html = try String(contentsOf: urlSite)
        result = Result.success(html)
    } catch {
        print(error.localizedDescription)
        resul = Result.failure(error)
    }
    return result
}

if let url = URL(string: "https://www.purplegiraffe.fr") {
    let purpleResult = obtenirHtml(urlSite: url)
    switch purpleResult {
    case .success(let purpleHtml):
        print(purpleHtml)
    case .failure(let error):
        print("ERROR")
        print(error.localizedDescription)
    }
}
